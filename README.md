# Gitlab runner helm chart

Configure and install gitlab runner helm chart with TLS enabled.

# Requirements

- [helm](https://helm.sh/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/) or [kind](https://kind.sigs.k8s.io/docs/user/quick-start/) or a
 similar tool for setting up a local kubernetes cluster
    - You can also use an existing remote cluster if you have access to one

# Configure helm chart

Configuration is stored in `values.yaml`.

Point the runner to the gitlab server of your choice and configure the registration token:
```shell script
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: "__REDACTED__"
```

Set resources limits, e.g:
```shell script
resources:
  limits:
    memory: 2Gi
    cpu: 1
```

configures 2Gi of memory and 1 cpu core.

Refer to [gitlab documentation](https://docs.gitlab.com/runner/#runner-registration) for more information.

## TLS

Chart enables TLS communication between docker service and build container
through `runners.config`. Ensure environment variables are set within the CI
to benefit from it, e.g. set variables at the group/project level in gitlab server:

```shell script
DOCKER_HOST=tcp://localhost:2376 
DOCKER_CERT_PATH=/certs/client 
DOCKER_TLS_VERIFY=1 
```

Notice that `DOCKER_HOST` points to `localhost` instead of `docker`, unlike when using
a docker executor for the gitlab runner.

# Set up local k8s cluster

To create a node with `4096` MB of memory and `4` cpus as a docker container in `minikube`:
```shell script
$ minikube start --memory=4096 --cpus=4 --driver=docker --addons='metrics-server'
```

Command above also configures a `metrics-server`, which allows to check resource consumption on a node with `top
` (useful to combine with `watch`):
```shell script
$ kubectl top nodes
NAME       CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
minikube   228m         1%     853Mi           2%
```

You can also use other drivers, e.g. `kvm2` (requires more configuration, see
[here](https://minikube.sigs.k8s.io/docs/drivers/kvm2/)):
```shell script
$ minikube start --memory=8g --cpus=8 --disk-size=60g --driver=kvm2
```

Keep in mind that when using `kvm2` the cluster will be running in a VM. Virtualization imposes an overhead and will
result in more resources being consumed - allocate more memory and cpu. Configure larger than default `disk-size` to
avoid disk pressure issues within the kubernetes cluster.

Another option is to create a cluster with `kind`, e.g:
```shell script
$ kind create cluster
```

Example above creates kubernetes in a docker container without imposing resource restrictions. 

# Install chart

Commands below assume you are in the project directory.

Pull gitlab runner chart:
```shell script
$ helm dependency update
```

Create a dedicated namespace `ci`:
```shell script
$ kubectl create namespace ci
```

Install the chart with release name `strong-ci` into namespace `ci`:
```shell script
$ helm upgrade -i strong-ci -n ci .
```

Confirm gitlab runner was installed:
```shell script
$ kubectl get all -n ci
NAME                                           READY   STATUS    RESTARTS   AGE
pod/strong-ci-gitlab-runner-6576cc668d-tfvtx   1/1     Running   0          24s

NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/strong-ci-gitlab-runner   1/1     1            1           24s

NAME                                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/strong-ci-gitlab-runner-6576cc668d   1         1         1       24s

```

# Basic debugging commands

Tail the gitlab runner logs to see job status and if the runner registered successfully, e.g:
```shell script
$ kubectl logs -f ci-gitlab-runner-9bdfc4d5f-zzbns 
Registration attempt 1 of 30
Runtime platform                                    arch=amd64 os=linux pid=13 revision=8fa89735 version=13.6.0
[...]                
Registering runner... succeeded                     runner=xxx
Merging configuration from template file "/scripts/config.template.toml" 
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
[...]
```

Execute into the build container when debugging jobs, e.g:
```shell script
$ kubectl exec -it runner-rn3dpek-project-19626392-concurrent-0rwgk8 -c build  -- /bin/sh
```

Check node status:
```shell script
$ kubectl describe nodes
Name:               minikube
Roles:              master
[...]
Events:
  Type     Reason                   Age                From        Message
  ----     ------                   ----               ----        -------
  Normal   Starting                 13m                kubelet     Starting kubelet.
  Normal   NodeHasSufficientMemory  13m (x3 over 13m)  kubelet     Node minikube status is now: NodeHasSufficientMemory
  Normal   NodeHasNoDiskPressure    13m (x3 over 13m)  kubelet     Node minikube status is now: NodeHasNoDiskPressure
  Normal   NodeHasSufficientPID     13m (x2 over 13m)  kubelet     Node minikube status is now: NodeHasSufficientPID
[...]
  Normal   NodeReady                13m                kubelet     Node minikube status is now: NodeReady
```

Refer to [kubernetes documentation](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-running-pod/)
for more information on how to debug pods and more.